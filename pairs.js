function pairs(obj) {
    array = [];
    if (obj == undefined) {
        return array;
    }
    for (i in obj) {
        temp = [i, obj[i]];
        array.push(temp);
    }
    return array;
}

module.exports = pairs;