function defaults(obj, defaultProps) {
    if (obj == undefined || defaultProps == undefined) {
        return {};
    }
    for (i in defaultProps) {
        if (!obj.hasOwnProperty(i)) {
            obj[i] = defaultProps[i];
        }
    }
    return obj;
}

module.exports = defaults;