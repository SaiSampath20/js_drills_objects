function mapObject(obj, cb) {
    object = {};
    if (cb == undefined || obj == undefined) {
        return object;
    }
    for (i in obj) {
        temp = cb(obj[i]);
        object[i] = temp;
    }
    return object;
}

module.exports = mapObject;