const values = require("../values");

const testObject = { name: 'Bruce Wayne', age: 36, 1: 'Gotham' };

// TestCase 1
const values_arr = values(testObject);

console.log(values_arr);

// TestCase 2
console.log(values([]));

// TestCase 3
console.log(values({}));

// TestCase 4
console.log(values());