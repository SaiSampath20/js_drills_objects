const defaults = require("../defaults");

let iceCream = { flavor: "chocolate" };

// TestCase - 1
const object = defaults(iceCream, { flavor: "vanilla", sprinkles: "lots" });

console.log(object);

// TestCase - 2
console.log(defaults({}));

// TestCase - 3
console.log(defaults());