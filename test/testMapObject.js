const mapObject = require("../mapObject");

const testObject = { name: 'Bruce Wayne', age: 36, 1: 'Gotham' };

// TestCase - 1
const object = mapObject(testObject, (val) => {
    if (typeof val != 'number')
        return val;
    return val + 5;
});

console.log(object);

// TestCase - 2
console.log(mapObject({}, (val) => {
    return val + 5;
}));

// TestCase - 3
console.log(mapObject({}));

// TestCase - 4
console.log(mapObject());