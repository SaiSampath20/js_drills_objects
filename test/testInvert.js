const invert = require("../invert");

// TestCase - 1
const testObject = { name: 'Bruce Wayne', age: 36, 2: 36, 1: 'Gotham' };

const object = invert(testObject);

console.log(object);

// TestCase - 2
console.log(invert({}));

// TestCase - 3
console.log(invert());