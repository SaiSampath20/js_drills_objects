const pairs = require("../pairs");

const testObject = { name: 'Bruce Wayne', age: 36, 1: 'Gotham' };

// TestCase 1
const pairs_arr = pairs(testObject);

console.log(pairs_arr);

// TestCase 2
console.log(pairs([]));

// TestCase 3
console.log(pairs({}));

// TestCase 4
console.log(pairs());