const keys = require("../keys");

const testObject = { name: 'Bruce Wayne', age: 36, 1: 'Gotham' };

// TestCase 1
const keys_arr = keys(testObject);

console.log(keys_arr);

// TestCase 2
console.log(keys([]));

// TestCase 3
console.log(keys({}));

// TestCase 4
console.log(keys());