function invert(obj) {
    newObj = {};
    if (obj == undefined) {
        return newObj;
    }
    for (i in obj) {
        newObj[obj[i]] = i;
    }
    return newObj;
}

module.exports = invert;